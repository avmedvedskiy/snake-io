﻿using UnityEngine;
using System.Collections;

public class WallObject : InteractObject {

    public override void Interact(Snake snake)
    {
        snake.Kill();
    }
}
