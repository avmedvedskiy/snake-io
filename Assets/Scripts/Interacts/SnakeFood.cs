﻿using UnityEngine;
using System.Collections;

public class SnakeFood : InteractObject
{

    public int score = 5;

    public override void Interact(Snake snake)
    {
        snake.AddChild();
        GameController.Instance.SnakeEat(snake, this);
        Destroy(gameObject);
    }
}
