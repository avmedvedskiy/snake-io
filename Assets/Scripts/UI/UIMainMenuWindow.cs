﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class UIMainMenuWindow : UIWindow
{
    public enum SceneSize
    {
        Small,
        Medium,
        Big
    }
    public SceneSize sceneSize = SceneSize.Small;
    public bool isAR = false;
    
    public void StartPlay()
    {
#if UNITY_EDITOR
        Debug.Log("StartPlay");
#endif
        SceneManager.LoadScene("GameScene" + sceneSize + (isAR?"AR":""));
        UIManager.Instance.ShowWindow(UIConstants.UIWindowNames.GAME_HUD);
        Hide();
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        Debug.Log("ExitGame");
#endif
        Application.Quit();
    }

    public void OnChangeSceneSize(Dropdown target)
    {
        sceneSize = (SceneSize)target.value;
    }

    public void OnToogleChange(Toggle target)
    {
        isAR = target.isOn;
    }

}
