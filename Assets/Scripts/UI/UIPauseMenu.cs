﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIPauseMenu : UIWindow
{
    public Text scoreText;

    public void ClosePause()
    {
        UIManager.Instance.ShowWindow(UIConstants.UIWindowNames.GAME_HUD);
        Hide();
    }

    public override void Show()
    {
        base.Show();
        UIManager.Instance.HideWindow(UIConstants.UIWindowNames.GAME_HUD);
        if (scoreText)
            scoreText.text = GameController.Instance.Score.ToString();

        GameController.Instance.inPause = true;
    }

    public override void Hide()
    {
        base.Hide();
        GameController.Instance.inPause = false;
    }

    public void Restart()
    {
        UIManager.Instance.Restart();
        Hide();
    }

    public void ToMainMenu()
    {
        UIManager.Instance.ToMainMenu();
        Hide();
    }

}
