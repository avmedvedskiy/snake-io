﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIGameHUD : UIWindow
{
    public Text scoreText;
    
    public override void Show()
    {
        base.Show();
        GameController.Instance.OnScoreChange += OnScoreChange;
        OnScoreChange();
    }

    public override void Hide()
    {
        base.Hide();
        GameController.Instance.OnScoreChange -= OnScoreChange;
    }

    public void SetPause()
    {
        UIManager.Instance.ToogleWindow(UIConstants.UIWindowNames.PAUSE_MENU);
    }

    public void OnScoreChange()
    {
        if (!scoreText)
            return;

        scoreText.text = GameController.Instance.Score.ToString();
    }
}
