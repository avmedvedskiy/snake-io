﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIGameOverMenu : UIWindow
{
    public Text score;
    public Text highScore;


    public override void Show()
    {
        base.Show();
        if (score)
            score.text = GameController.Instance.Score.ToString();

        if (highScore)
            highScore.text = GameController.Instance.GetHighScore().ToString();

        UIManager.Instance.HideWindow(UIConstants.UIWindowNames.GAME_HUD);

        GameController.Instance.inPause = true;
    }

    public override void Hide()
    {
        base.Hide();
        GameController.Instance.inPause = false;
    }

    public void Restart()
    {
        UIManager.Instance.Restart();
        Hide();
    }

    public void ToMainMenu()
    {
        UIManager.Instance.ToMainMenu();
        Hide();
    }
}
