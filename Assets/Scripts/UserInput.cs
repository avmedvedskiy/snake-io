﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class UserInput : MonoBehaviour {

    protected Snake m_Snake;
    public Snake Snake
    {
        get { return m_Snake ?? (m_Snake = GetComponent<Snake>()); }
    }
	
	void Update ()
    {

        if (GameController.Instance.inPause)
            return;

        UpdateMove();
        UpdateBuster();
        UpdateTestInputs();

    }

    void UpdateTestInputs()
    {
        //if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        //    Snake.AddChild();

        //if (CrossPlatformInputManager.GetButtonDown("Fire2"))
        //    Snake.RemoveChild();
    }

    void UpdateMove()
    {
        Snake.Motor.moveVector.x = CrossPlatformInputManager.GetAxis("Horizontal");
        Snake.Motor.moveVector.y = CrossPlatformInputManager.GetAxis("Vertical");
    }

    void UpdateBuster()
    {
        Snake.Motor.inBuster = CrossPlatformInputManager.GetButton("Buster");
    }
}
