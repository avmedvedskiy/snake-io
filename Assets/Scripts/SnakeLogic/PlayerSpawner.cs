﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;

public class PlayerSpawner : MonoBehaviour {

    public GameObject playerSnake;
    public FreeLookCam playerCamera;

    // Use this for initialization
    void Start ()
    {
        if (playerSnake == null)
            return;

        var snakeGo = Instantiate(playerSnake);
        snakeGo.transform.position = transform.position;
        snakeGo.transform.rotation = transform.rotation;
        snakeGo.transform.parent = transform.parent;
        
        if(playerCamera != null)
        {
            var camera = Instantiate(playerCamera);
            camera.SetTarget(snakeGo.transform.GetChild(0));
        }
    }
}
