﻿using UnityEngine;
using System.Collections.Generic;

public class SnakeMotor : MonoBehaviour
{
    public float idleSpeed = 0.5f;
    public float speed = 2f;
    public float busterSpeed = 4f;
    public float rotationSpeed = 2f;

    public bool inBuster = false;

    public Vector3 moveVector = Vector3.zero;

    [Space(10)]
    public float currentSpeed;

    public void UpdateMove()
    {

        Vector3 forward = Camera.main.transform.TransformDirection(Vector3.forward);

        if(Camera.main.transform.eulerAngles.x == 90f) // камера находится ровно в вертикальном положении
            forward = Camera.main.transform.TransformDirection(Vector3.up);

        forward.y = 0f;
        forward = forward.normalized;
        Vector3 right = new Vector3(forward.z, 0.0f, -forward.x);

        Vector3 walkDirection = (moveVector.x * right + moveVector.y * forward) + transform.forward * idleSpeed;
        walkDirection = Vector3.ProjectOnPlane(walkDirection, Vector3.up);
        Vector3 tempDirection = Vector3.Lerp(transform.forward, walkDirection, Time.deltaTime * rotationSpeed);
        transform.rotation = Quaternion.LookRotation(tempDirection);

        float moveSpeed = Mathf.Clamp01(walkDirection.magnitude);
        currentSpeed = moveSpeed * ((inBuster) ? busterSpeed : speed);
        transform.Translate(Vector3.forward * currentSpeed * Time.deltaTime);

        Debug.DrawRay(transform.position, walkDirection);

    }

    public void UpdateMoveNodes(List<Node> nodes, Node root)
    {

        const float SPEED_FACTOR = 0.9f;
        nodes[0].smoothTime = SPEED_FACTOR * 1 / currentSpeed;
        nodes[0].Move(root);

        for (int i = 1; i < nodes.Count; i++)
        {
            nodes[i].smoothTime = SPEED_FACTOR * 1 / currentSpeed;
            nodes[i].Move(nodes[i - 1]);
        }
    }

}
