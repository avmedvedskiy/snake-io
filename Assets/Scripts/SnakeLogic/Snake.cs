﻿using UnityEngine;
using System.Collections.Generic;

public class Snake : MonoBehaviour
{
    public Node root;
    public List<Node> childNodes = new List<Node>();

    public Collider headCollider;

    protected SnakeMotor m_Motor;
    public SnakeMotor Motor
    {
        get { return m_Motor ?? (m_Motor = GetComponent<SnakeMotor>()); }
    }

    void Start ()
    {
	
	}
	
	void Update ()
    {
        if (GameController.Instance.inPause)
            return;

        //if not visible by Vuforia
        if (headCollider != null && headCollider.enabled == false)
            return;

        Motor.UpdateMove();
        Motor.UpdateMoveNodes(childNodes, root);
    }
    

    public void AddChild()
    {
        var newChild = Instantiate(root);
        newChild.name = root.name;
        newChild.transform.parent = transform.parent;
        var lastNode = childNodes[childNodes.Count - 1];
        newChild.transform.position = lastNode.transform.position;
        childNodes.Add(newChild);
    }

    public void RemoveChild()
    {
        if (childNodes.Count <= 1)
            return;

        var lastChild = childNodes[childNodes.Count - 1];

        if (lastChild == null)
            return;

        childNodes.Remove(lastChild);
        Destroy(lastChild.gameObject);
    }

    public void Kill()
    {
        enabled = false;
        GameController.Instance.SnakeKill();
    }

    void OnTriggerEnter(Collider collider)
    {
        var interact = collider.GetComponent<InteractObject>();
        if (interact == null)
            return;

        interact.Interact(this);
    }

}
