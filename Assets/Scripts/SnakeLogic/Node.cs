﻿using UnityEngine;
using System.Collections;

public class Node : MonoBehaviour
{
    /// <summary>
    /// Погрешность для изменения скорости (меньше 1 ноды будут ближе друг к другу, больше - дальше друг от друга)
    /// </summary>
    public const float SPEED_FACTOR = 0.95f;
    
    public float smoothTime = 0.5f;

    private Vector3 m_MovementVelocity;
    public void Move(Node node)
    {
        transform.position = Vector3.SmoothDamp(transform.position, node.transform.position,ref m_MovementVelocity, smoothTime);
        transform.LookAt(node.transform);
    }


    /// <summary>
    /// Скорость Node имеет обратно пропорциональную зависимость от скорости перемещения рута.
    /// </summary>
    /// <param name="speed"></param>
    public void SetSpeed(float speed)
    {
        smoothTime = SPEED_FACTOR / speed;
    }
}
