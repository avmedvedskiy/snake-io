﻿using UnityEngine;
using System.Collections;
using System;

public class GameController : Singleton<GameController>
{
    private int m_Score;
    public int Score
    {
        get { return m_Score; }
        set
        {
            m_Score = value;
            SafeCall(OnScoreChange);
        }
    }

    public bool inPause = false;

    public Action OnScoreChange;
    public Action OnSnakeEat;
    public Action OnSnakeKill;

    public Action OnGameOver;
    

    public void SnakeKill()
    {
        GameOver();
        SafeCall(OnSnakeKill);
    }

    public void SnakeEat(Snake snake, SnakeFood food)
    {
        Score += food.score;
        SafeCall(OnSnakeEat);
    }

    public void GameOver()
    {
        Debug.Log("GameOver. Scores : " + Score);
        SaveHighScore();
        SafeCall(OnGameOver);
    }

    public void Restart()
    {
        Score = 0;
    }

    const string HIGH_SCORE_KEY = "HighScoreKey";
    public int GetHighScore()
    {
        if (PlayerPrefs.HasKey(HIGH_SCORE_KEY))
            return PlayerPrefs.GetInt(HIGH_SCORE_KEY);
        else
            return 0;
    }

    public void SaveHighScore()
    {
        int highScore = GetHighScore();
        if(Score > highScore)
            PlayerPrefs.SetInt(HIGH_SCORE_KEY,Score);

        PlayerPrefs.Save();
    }


    public static void SafeCall(Action a)
    {
        if (a != null)
            a.Invoke();
    }

}
